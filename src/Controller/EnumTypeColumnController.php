<?php

namespace Drupal\enum_type_column\Controller;

use Drupal\enum_type_column\Model\EnumTypeColumnModel;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Event handler of module.
 */
class EnumTypeColumnController {

  /**
   * Initial page.
   *
   * @return array
   *   A renderable array.
   */
  public function index() {
    $table_details = EnumTypeColumnModel::processData();
    return [
      '#theme' => 'enum_type_column_describe_tables',
      '#content' => $table_details,
    ];
  }

  /**
   * Apply enum column type action.
   *
   * @return array
   *   A renderable array.
   */
  public function apply($table, $column, $data_type) {
    global $base_url;
    $confirm_arr = [$table, $column, $data_type];
    $confirm_url = implode(',', $confirm_arr);
    $confirm_url_encode = $base_url . '/confirm/enum_type/' . base64_encode($confirm_url);

    $column_data = EnumTypeColumnModel::columnData($table, $column);
    if (count($column_data) == 0) {
      $form = \Drupal::formBuilder()->getForm('\Drupal\enum_type_column\Form\EnumTypeValuesForm');
    }

    $data = [
      'table' => $table,
      'column' => $column,
      'data' => $column_data,
      'form' => $form,
      'data_type' => $data_type,
      'confirm' => $confirm_url_encode,
    ];

    return ['#theme' => 'enum_type_column_populate_column_data', '#content' => $data];
  }

  /**
   * Revert enum type column change.
   *
   * @return array
   *   A renderable array.
   */
  public function revert($table, $column, $data_type) {
    $form = \Drupal::formBuilder()->getForm('\Drupal\enum_type_column\Form\EnumTypeColumnRevertForm');
    return $form;
  }

  /**
   * Confirm enum type column change action.
   *
   * @return array
   *   A renderable array.
   */
  public function confirmChange($url) {
    $decode_url = base64_decode($url);
    $arr = explode(',', $decode_url);
    $table = $arr[0];
    $column = $arr[1];
    $data_type = $arr[2];
    $mission_accomplished = EnumTypeColumnModel::performEnumOperation($table, $column);
    if ($mission_accomplished) {
      drupal_set_message(t("Column changes to enum type successfully"));
    }
    else {
      drupal_set_message(t('Query did not executed properly.'), 'error');
    }
    return new RedirectResponse(\Drupal::url('enum_type_column.settings', [], ['absolute' => TRUE]));
  }

  /**
   * Historical enum changes of a column.
   *
   * @return array
   *   A renderable array.
   */
  public function history($table, $column, $data_type) {
    $header = [
      [
        'data' => 'Table name',
        'field' => 'w.table_name',
      ],
      [
        'data' => 'Column',
        'field' => 'w.column_name',
      ],
      [
        'data' => 'Data type',
        'field' => 'w.data_type',
      ],
      [
        'data' => 'Updated',
        'field' => 'w.updated',
      ],
    ];

    $query = \Drupal::database()->select('enum_type_column', 'w')
      ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
      ->extend('\Drupal\Core\Database\Query\TableSortExtender');
    $query->fields('w', [
      'table_name',
      'column_name',
      'data_type',
      'updated',
    ]);

    $query->condition('w.table_name', $table);
    $query->condition('w.column_name', $column);

    $result = $query
      ->limit(10)
      ->orderByHeader($header)
      ->execute();

    foreach ($result as $data) {
      $rows[] = [
        'data' => [
          $data->table_name,
          $data->column_name,
          $data->data_type,
          date("F j, Y, g:i a", $data->updated),
        ],
      ];
    }
    $build['table_data'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => 'No log messages available.',
    ];
    $build['table_pager'] = ['#type' => 'pager'];
    return $build;
  }

}

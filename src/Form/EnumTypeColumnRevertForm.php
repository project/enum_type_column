<?php

namespace Drupal\enum_type_column\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\enum_type_column\Model\EnumTypeColumnModel;

/**
 * Revert enum type on a column.
 */
class EnumTypeColumnRevertForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'enum_type_column_revert_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $current_path = \Drupal::service('path.current')->getPath();
    $args = explode('/', $current_path);
    $table_name = $args[3];
    $form['table_name'] = [
      '#type' => 'textfield',
      '#title' => 'Table name',
      '#default_value' => $table_name,
      '#disabled' => TRUE,
    ];
    $column_name = $args[4];
    $form['column_name'] = [
      '#type' => 'textfield',
      '#title' => 'Column Name',
      '#default_value' => $column_name,
      '#disabled' => TRUE,
    ];
    $default_data_type = $args[5];
    $connection = \Drupal::database();
    $query = $connection->query("SELECT DISTINCT(data_type) FROM {enum_type_column} WHERE column_name = :column AND table_name = :table_name AND data_type NOT LIKE '%enum%'", [':column' => $column_name, ':table_name' => $table_name]);
    $data_types = $query->fetchAll();
    if (count($data_types) > 0) {
      foreach ($data_types as $key => $value) {
        $data_types_arr[$value->data_type] = $value->data_type;
      }
      $form['data_type'] = [
        '#type' => 'select',
        '#title' => 'Data Type',
        '#options' => $data_types_arr,
      ];
      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#button_type' => 'primary',
      ];
    }
    else {
      $form['data_type'] = [
        '#type' => 'textfield',
        '#title' => 'Data Type',
        '#default_value' => $default_data_type,
        '#disabled' => TRUE,
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $table = $form_state->getValue('table_name');
    $column = $form_state->getValue('column_name');
    $data_type = $form_state->getValue('data_type');
    $mission_accomplished = EnumTypeColumnModel::revertEnumType($table, $column, $data_type);
    if ($mission_accomplished) {
      drupal_set_message(t("Enum type column reverted successfully"));
    }
    else {
      drupal_set_message(t('Query did not executed properly.'), 'error');
    }

    $form_state->setRedirect('enum_type_column.settings');
  }

}

<?php

namespace Drupal\enum_type_column\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\enum_type_column\Model\EnumTypeColumnModel;

/**
 * Form for create and edit enum values for a column.
 */
class EnumTypeValuesForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'enum_type_values_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $current_path = \Drupal::service('path.current')->getPath();
    $args = explode('/', $current_path);
    $table_name = $args[3];
    $form['table_name'] = [
      '#type' => 'hidden',
      '#default_value' => $table_name,
    ];
    $column_name = $args[4];
    $form['column_name'] = [
      '#type' => 'hidden',
      '#default_value' => $column_name,
    ];

    $column_structure = EnumTypeColumnModel::columnStructure($table_name, $column_name);
    $enum_type = $column_structure->Type;
    $enum_str_string = '';
    if (strpos($enum_type, 'enum') !== FALSE) {
      $enum_str_format = str_replace('enum(', '', $enum_type);
      $enum_str_format = str_replace("')", "'", $enum_str_format);
      $enum_str_arr = explode(',', $enum_str_format);
      foreach ($enum_str_arr as $key => $value) {
        $enum_str_arr_format[] = str_replace("'", "", $value);
      }
      $enum_str_string = implode("\n", $enum_str_arr_format);
    }

    $form['enum_field_value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enum values'),
      '#description' => $this->t('Enter one value per line'),
      '#required' => TRUE,
      '#default_value' => $enum_str_string,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $enum_values = trim($form_state->getValue('enum_field_value'));
    if (empty($enum_values)) {
      form_set_error('enum_field_value', t('Blank values are not allowed'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $enum_values = trim($form_state->getValue('enum_field_value'));
    $enum_values = explode("\n", $enum_values);
    foreach ($enum_values as $key => $value) {
      $default_value[] = "'" . trim($value) . "'";
    }
    $table = $form_state->getValue('table_name');
    $column = $form_state->getValue('column_name');
    $mission_accomplished = EnumTypeColumnModel::performEnumOperation($table, $column, $default_value);
    if ($mission_accomplished) {
      drupal_set_message(t("Column changes to enum type successfully"));
    }
    else {
      drupal_set_message(t('Query did not executed properly.'), 'error');
    }

    $form_state->setRedirect('enum_type_column.settings');
  }

}

<?php

namespace Drupal\enum_type_column\Model;

use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Contains database operation on module table.
 */
class EnumTypeColumnModel {

  /**
   * Provide all the data of tables.
   *
   * @return array
   *   Prepares module list.
   */
  public static function processData() {
    // Get all module list.
    $modules_list = \Drupal::ModuleHandler()->getModuleList();
    foreach ($modules_list as $name => $module) {
      $module_path = $module->getPathname();
      if (strpos($module_path, 'modules/custom') !== FALSE) {
        $module_find_path = DRUPAL_ROOT . '/modules/custom/' . $name . '/' . $name . '.install';
        if (!file_exists($module_find_path)) {
          continue;
        }
        require_once $module_find_path;
        $schema_fx = $name . '_schema';
        $schema_data[] = $schema_fx();
      }
    }
    $injected_database = \Drupal::database();
    foreach ($schema_data as $key => $table_schema) {
      foreach ($table_schema as $table_name => $table_fields) {
        if ($table_name == 'enum_type_column') {
          continue;
        }

        $query = $injected_database->query("SHOW FULL COLUMNS FROM $table_name");
        $results = $query->fetchAll();
        foreach ($results as $result_key => $result_value) {
          if ($result_value->Null == 'NO' && $result_value->Key != 'PRI') {
            $table_details[$table_name][] = [
              'field' => $result_value->Field,
              'type' => $result_value->Type,
              'null' => $result_value->Null,
              'default' => $result_value->Default,
              'comment' => $result_value->Comment,
            ];
          }
        }
      }
    }
    return $table_details;
  }

  /**
   * Provide column data from table.
   *
   * @return array
   *   Columns list of a table.
   */
  public static function columnData($table_name, $column_name) {
    $connection = \Drupal::database();
    $query = $connection->query("SELECT DISTINCT($column_name) FROM {$table_name}");
    $result = $query->fetchAll();
    return $result;
  }

  /**
   * Provides column structure from specified table.
   *
   * @return object
   *   Column structure of a table.
   */
  public static function columnStructure($table_name, $column_name) {
    $connection = \Drupal::database();
    $query = $connection->query("SHOW FULL COLUMNS FROM {$table_name} WHERE Field = '$column_name'");
    $result = $query->fetchObject();
    return $result;
  }

  /**
   * Performs changes in table structure for specified column.
   *
   * @return bool
   *   Return status of enum operation.
   */
  public static function performEnumOperation($table, $column, $default_enum_values = NULL) {
    if ($default_enum_values != NULL) {
      $column_values = $default_enum_values;
    }
    else {
      $column_data = self::columnData($table, $column);
      foreach ($column_data as $key => $value) {
        $column_values[] = "'" . $value->{$column} . "'";
      }
    }

    $column_values = array_unique($column_values);
    $enum_values = implode(',', $column_values);

    $column_structure = self::columnStructure($table, $column);
    if ($column_structure->Null == 'NO') {
      $null_string = 'NOT NULL';
    }
    else {
      $null_string = 'NULL';
    }

    $query = "ALTER TABLE $table CHANGE $column $column ENUM($enum_values) $null_string";

    $default_string = '';
    if ($column_structure->Default != '') {
      $default_string = $column_structure->Default;
      $default_value_include = "'" . $default_string . "'";
      array_unshift($column_values, $default_value_include);
      $column_values = array_unique($column_values);
      $enum_values = implode(',', $column_values);
      $query = "ALTER TABLE $table CHANGE $column $column ENUM($enum_values) $null_string DEFAULT '$default_string'";
    }

    $comment_string = '';
    if (!empty($column_structure->Comment)) {
      $comment_string = $column_structure->Comment;
      $query = "ALTER TABLE $table CHANGE $column $column ENUM($enum_values) $null_string COMMENT '$comment_string'";
    }

    if ($column_structure->Default != '' && !empty($column_structure->Comment)) {
      $query = "ALTER TABLE $table CHANGE $column $column ENUM($enum_values) $null_string DEFAULT '$default_string' COMMENT '$comment_string'";
    }

    try {
      $injected_database = \Drupal::database();
      $injected_database->query($query)->execute();
    }
    catch (DatabaseExceptionWrapper $e) {
      $message = $e->getMessage();
      drupal_set_message($message, 'error');
      return FALSE;
    }

    $uid = \Drupal::currentUser()->id();
    $fields = [
      'table_name' => $table,
      'column_name' => $column,
      'data_type' => $column_structure->Type,
      'created' => time(),
      'updated' => time(),
      'createdby' => $uid,
      'updatedby' => $uid,
    ];
    db_insert('enum_type_column')->fields($fields)->execute();
    return TRUE;
  }

  /**
   * Revert enum type changes for specified column.
   *
   * @return bool
   *   Return status of revert.
   */
  public static function revertEnumType($table, $column, $data_type) {
    $column_structure = self::columnStructure($table, $column);
    if ($column_structure->Null == 'NO') {
      $null_string = 'NOT NULL';
    }
    else {
      $null_string = 'NULL';
    }
    $query = "ALTER TABLE $table CHANGE $column $column $data_type $null_string";

    $default_string = '';
    if ($column_structure->Default != '') {
      $default_string = $column_structure->Default;
      $query = "ALTER TABLE $table CHANGE $column $column $data_type $null_string DEFAULT '$default_string'";
    }

    $comment_string = '';
    if (!empty($column_structure->Comment)) {
      $comment_string = $column_structure->Comment;
      $query = "ALTER TABLE $table CHANGE $column $column $data_type $null_string COMMENT '$comment_string'";
    }

    if ($column_structure->Default != '' && !empty($column_structure->Comment)) {
      $query = "ALTER TABLE $table CHANGE $column $column $data_type $null_string DEFAULT '$default_string' COMMENT '$comment_string'";
    }
    try {
      $injected_database = \Drupal::database();
      $injected_database->query($query)->execute();
    }
    catch (DatabaseExceptionWrapper $e) {
      $message = $e->getMessage();
      drupal_set_message($message, 'error');
      return FALSE;
    }
    return TRUE;
  }

}
